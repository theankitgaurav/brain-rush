$(function () { //Ensuring the DOM is ready
  /////////////////////////////////////////////////
  //Starting game
  $('.level1').click(function () {
    startGame(1);
  });
  $('#replay').click(function () {
    startGame(1);
  });
  $('#level2').click(function () {
    startGame(2);
  });
  //Declaring the global variables
  score = 0;
  quesCount = 0;
  correctCount = 0;
  wrongCount = 0;
  highScore = getHighScore();
  $('#high_score').html('Highest Score: ' + highScore);

  //Function to get high score from cookie
  function getHighScore() {
    return localStorage.getItem('highScore');
  }
  //Function to set high score to cookie

  function setHighScore(x) {
    localStorage.setItem('highScore', x);
  }
  //Function to generate random numbers for the questions

  function randNum() {
    var num = 0;
    num = Math.floor((Math.random()) * 11);
    // if(num<20)
    // 	return num;
    // else
    // 	return randNum();
    return num;
  }

  //Defining class for the option button
  function block(a, b) {
    this.string = a + ' + ' + b;
    this.value = a + b;
    this.createBlock = function (x) {
      document.getElementById(x).innerHTML = this.string;
    }
  }

  //Function to check whether user choice is right or wrong
  function checkAns1() {
    window.clearTimeout(timer)
    myVideo.play();
    if (b1.value >= b2.value) { //right answer condition
      correctCount++;
      score += 10;
      $('#score').html('Total Score: ' + score);
      $('#first').removeClass('btn-warning').addClass('btn-success animated bounce');
      setTimeout(function () {
        showBlocks();
      }, 500);
    } 
    else { //wrong answer condition
      wrongCount++;
      score = score - 5;
      $('#score').html('Total Score: ' + score);
      $('#first').removeClass('btn-warning').addClass('btn-danger animated bounce');
      setTimeout(function () {
        showBlocks();
      }, 500);
    }
  }
  function checkAns2() {
    window.clearTimeout(timer)
    myVideo.play();
    if (b2.value >= b1.value) { //right answer condition
      correctCount++;
      score += 10;
      $('#score').html('Total Score: ' + score);
      $('#second').removeClass('btn-warning').addClass('btn-success animated bounce');
      setTimeout(function () {
        showBlocks();
      }, 500);
    } 
    else { //wrong answer condition
      wrongCount++;
      score = score - 5;
      $('#score').html('Total Score: ' + score);
      $('#second').removeClass('btn-warning').addClass('btn-danger animated bounce');
      setTimeout(function () {
        showBlocks();
      }, 500);
    }
  }

  //Function to show the questions
  function showBlocks() {
    quesCount++;
    if (quesCount <= 10) { //showing questions if level not complete
      $('#options_block').show(200);
      $('#second, #first').removeClass('btn-danger btn-success animated bounce').addClass('btn-warning');
      
      // Creating the options
      b1 = new block(randNum(), randNum());
      b1.createBlock('first');
      b2 = new block(randNum(), randNum());
      b2.createBlock('second');

      //waiting for response from user
      //skipping to next question after 1 sec
      timer = setTimeout(function () {
        $('#options_block').hide().show(300);
        myVideo.play();
        showBlocks();
      }, 2000);
      document.getElementById('first').onclick = checkAns1;
      document.getElementById('second').onclick = checkAns2;
    } 
    else { //Showing results of completed level
      showResult();
    }
  }
  //Function to show result of completed level

  function showResult() {
    var time = Math.round((Date.now() - now) / 1000);
    var accuracy = (correctCount * 10) - (wrongCount * 5);
    if (score >= highScore) {
      highScore = score;
      setHighScore(score);
      $('#high_score').html('Highest Score: ' + highScore);
    }
    var result = '<div class=\'col-md-6\'><h4>Level 1 results:</h4><div class=\'label label-success\'>Correct = ' + correctCount + '</div><br><div class=\'label label-danger\'>Wrong = ' + wrongCount + '</div><br><div class=\'label label-warning\'>Skipped = ' + (quesCount - wrongCount - correctCount - 1) + '</div><br>Accuracy: ' + accuracy + ' %<br>Total Questions: ' + (quesCount - 1) + '</div>';
    $('#pallette').html(result);
  }
  //Function to initiate the game

  function startGame(x) {
    now = Date.now();
    $('#start').hide();
    showBlocks();
  }
  /////////

});
